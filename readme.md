### Overview

This project aims to be a data processing and visualization framework/lib. 

Project also hosts particular statistics using this framework. 

The framework allows you to read/parse/process/convert data from different *structured* data sources across the internet.

Data source can be a webservice, a table in a webpage, a `.csv` `.xlsx` `.xml` or `.json` file.

We favour the `json` data format and `Python3` as a programming language.
  (see the source files of http://convertjson.com/ or https://docs.python.org/3/library/json.html)
We prefer PyCharm as an IDE, and JuPyter and ThreeJS as a visulaization tools.
(https://github.com/jupyter-widgets/pythreejs)

Note: manuscript < printed material < digital document < structured digital document.

### Datasources:
    1. ksh.hu
    2. ...

### Project Structure

Folders:
```
    consent
        framework
            converter

        goals
        
    data (in .gitignore)
    community
        issues
        goals
```
### Workflow of Contribution

Issues/goals should be maintained within this project. (No external issue tracker.)

Issues/goals have an extra final phase dedicated to admins: integrate. This could also include the deletion of the particular community subfolder
(like deleting the feature branch in a normal git workflow).

This workflow would avoid merging difficulties on the public side and pushes this work toward admins (integrate phase of issues).

Anyone should have rights to contribute. (See git / cloning, feature branches, pull requests.)



PR/MR must be either a fix or just an extension (TODO autocheck). No mixtures! 

Extensions points to the community folder only and can be merged automatically.

Authors should commit only in the following folders (TODO autocheck): 
`community/issues/issueId/author`
`community/goals/goalId/StatisticsCategory/goalId/author-suffix`.

Fixes could point to anywhere else in the code and should follow the normal git feature branch workflow.



Voting mechanism should be introduced to rank amongst goals and issues.

Anyone can introduce a goal but only admins can introduce an issue.

Malicious admins would result in alternative forks and communities. 


We suggest the following statistical categories:
```
    regressions
    comparisons
    ...
```
### Issues FAQ

https://docs.gitlab.com/ee/user/project/issue_board.html

Epic: Use if there are multiple discussions throughout different issues created in distinct projects within a Group.
    Epic belongs to a group or subgroup. You can create from the group page. Epic may contain child epics or issues.
Milestone: Organize issues and merge requests into a cohesive group, with an optional start date and an optional due date.
Issue: ....
Issue list: column in a board based on a label. Not every label has a list. Every list corresponds to a label. Gitlab treats list-labels az mutually exclusive (if I move the issue on the board).

### TODO
    1. Autchecks mentioned above
    2. Sandbox contributions should trigger state changes in the issue of the master project
    3. JavaScript, Python, Scala cooperation ?
    4. Create some issues
    5. Licensing

### Sources
https://github.community/t5/Support-Protips/The-difference-between-forking-and-cloning-a-repository/ba-p/1372
  (Issues, branches, pull requests and other features will not copy over to your fork!)

https://docs.gitlab.com/ee/raketasks/backup_restore.html
    backup (with issues)
    
https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet    
